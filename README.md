# InternetDevels template for Drupal 8 projects

## Template statements

- Define a template that can easily be used with or without Docker (but is Docker-ready).
- Define a template that can be used in any HTTP environment (ngnix, Apache, IIS).
- It should be based on drupal-composer/drupal-project.
- It should use composer + bower to manage PHP and JS dependencies.
- `settings.php` stores only global configuration. Environment specific settings are overriden in `settings.local.php` files.

## Installation

1. Install composer:
   [https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
1. Install bower:
   `npm install -g bower`
1. Install docker-compose:
   [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)
1. Run `docker-compose up -d`
1. Run `composer install`
1. Copy `web/sites/example.settings.local.php` file to `web/sites/default/settings.local.php` file.
1. Add following lines to `web/sites/default/settings.local.php`:
```
$databases['default']['default'] = array (
  'database' => 'drupal',
  'username' => 'drupal',
  'password' => 'drupal',
  'host' => 'mariadb',
  'port' => '3306',
  'driver' => 'mysql',
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
);
```
1. Run `docker-compose exec php bin/drupal site:install --root=/var/www/html/web`

## Usage

* Drush commands can be executed with `docker-compose exec php bin/drush [command] [options] --root=/var/www/html/web`
* Drupal console commands can be executed with `docker-compose exec php bin/drupal [command] [options] --root=/var/www/html/web`
* To install a Drupal module, use: `composer require drupal/[module_machine_name]:[version]`
* To install a Javascript library, use: `bower install --save [library-name]`
