<?php

namespace Drupal\contact_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContactForm
 * @package Drupal\contact_form\Form
 */

class ContactForm extends FormBase {
  /**
   * {@inheritdoc}
   * @return string
   * @Description: to do.
   */

  public function getFormId() {
    return 'contact_form_custom';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Full name.
    $form['full_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Full name'),
      '#required' => TRUE,
    );
    // Email.
    $form['email'] = array(
      '#type' => 'email',
      '#title' => t('Email'),
      '#required' => TRUE,
    );
    // Phone number.
    $form['phone_number'] = array(
      '#type' => 'tel',
      '#title' => t('Phone'),
      '#description' => t('Use phone number format +38-000-00-00-000'),
      '#required' => TRUE,
      '#pattern' => '\+38-[0-9]{3}-[0-9]{2}-[0-9]{2}-[0-9]{3}',
    );
    // Subject.
    $form['subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#required' => TRUE,
    );
    // Message.
    $form['message'] = array(
      '#type' => 'textarea',
      '#title' => t('Message'),
      '#required' => TRUE,
      '#placeholder' => t('Type your message here'),
    );
    // Submit.
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Check subject.
    if (strlen($form_state->getValue('subject')) <= 3) {
      $form_state->setErrorByName(
        'subject',
        $this->t('Wrong subject format')
      );
    }
    // Check full name.
    if (strlen($form_state->getValue('full_name')) <= 3) {
      $form_state->setErrorByName(
        'full_name',
        $this->t('Wrong full name')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $mailManager = \Drupal::service('plugin.manager.mail');

    $module = 'contact_form';
    $key = 'contact_form';
    $to = $form_state->getValue('email');
    $params['subject'] = $form_state->getValue('subject');
    $params['message'] = $form_state->getValue('message');
    $params['full_name'] = $form_state->getValue('full_name');
    $params['phone_number'] = $form_state->getValue('phone_number');
    $send = true;

    $result = $mailManager->mail($module, $key, $to, TRUE, $params,  NULL, $send);
    if ($result['result'] !== true) {
      drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
    }
    else {
      drupal_set_message(t('Your message has been sent.'));
    }
  }
}
