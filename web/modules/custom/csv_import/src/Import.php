<?php

namespace Drupal\csv_import;

use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;

/**
 * Class Import
 * @package Drupal\custom_csv_import\Import.
 */
class Import {
  protected $fid;
  protected $file;
  protected $delimiter;
  protected $enclosure;
  /**
   * {@inheritdoc}
   */
  public function __construct($fid, $delimiter = ";", $enclosure = ",") {
    $this->fid = $fid;
    $this->delimiter = $delimiter;
    $this->enclosure = $enclosure;
  }

  /**
   * {@inheritdoc}
   */
  public function loadFile() {
    $this->file = File::load($this->fid);
  }

  /**
   * {@inheritdoc}
   */
  public function readFile() {
    $this->loadFile();
    $data = array();
    if (($handle = fopen($this->file->getFileUri(), "r")) !== FALSE) {
      while (($line = fgetcsv($handle, 0, $this->delimiter)) !== FALSE) {
        $data[] = $line;
      }
      fclose($handle);

      return $data;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function savePhoto($url) {
    $data = file_get_contents($url);
    $file = file_save_data($data, 'public:// image-node-[nid].png', FILE_EXISTS_REPLACE);

    return $file;
  }

  /**
   * @param $title
   * @param $body
   * @param $image_url
   * @param $price
   * @param $color
   * @param $category
   */
  public function insertNode($title, $body, $image_url, $price, $color, $category) {
    if ($title !== NULL) {
      $image = $this->savePhoto($image_url);

      $node = Node::create([
        'type' => 'product',
        'langcode' => 'en',
        'status' => 1,
      ]);
      $node->title = $title;
      $node->body = [
        'value' => $body,
        'format' => 'full_html',
      ];
      $node->field_image_product = [
        'target_id' => $image->id(),
      ];
      $node->field_price = $price;
      $node->field_color = $color;
      $node->field_category = $category;

      $node->save();

      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getTagId($vocabulary, $name) {
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vocabulary);
    $query->condition('name', $name);
    $result = $query->execute();

    $tid = reset($result);

    return $tid;
  }

  /**
   * {@inheritdoc}
   */
  public function import() {
    $data = $this->readFile();
    if ($data) {
      foreach ($data as $item) {
        $item[4] = $this->getTagId('color', $item[4]);
        $item[5] = $this->getTagId('category', $item[5]);

        $this->insertNode($item[0], $item[1], $item[2], $item[3], $item[4], $item[5]);
      }
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}
