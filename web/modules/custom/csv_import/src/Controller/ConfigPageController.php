<?php

namespace Drupal\csv_import\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class ConfigPageController.
 *
 * @package Drupal\csv_import
 */
class ConfigPageController extends ControllerBase {
  /**
   * {@inheritdoc}
   */

  public function showPage() {
    $output = array();
    $form_class = '\Drupal\csv_import\Form\ImportForm';

    $output['#markup'] = 'Test';
    $output['form'] = \Drupal::formBuilder()->getForm($form_class);

    return $output;
  }

}
