<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.03.17
 * Time: 16:14
 */

namespace Drupal\csv_import\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\Component\Render\FormattableMarkup;

/**
 * @DsField(
 *   id = "word_count",
 *   title = @Translation("DS: Word count"),
 *   provider = "csv_import",
 *   entity_type = "node",
 *   ui_limit = {"product|full"}
 * )
 */
class WordCount extends DsFieldBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $entity = $this->entity();

    if ($body_value = $entity->body->value) {
      return [
        '#type' => 'markup',
        '#markup' => new FormattableMarkup(
          '<strong>Count word: </strong> @word_count',
          [
            '@word_count' => str_word_count(strip_tags($body_value))
          ]
        )
      ];
    }
  }
}